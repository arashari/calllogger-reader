package dev.tetamba.callloggerreader;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import javax.annotation.Nullable;

import dev.tetamba.callloggerreader.models.ListItem;
import dev.tetamba.callloggerreader.models.LogItem;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    BaseApp app;
    ListView lv;
    ArrayAdapter adapter;
    DateFormat df;
    GoogleMap gMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        app = BaseApp.getInstance();
        lv = findViewById(R.id.lv_root);
        df = SimpleDateFormat.getDateInstance();

        adapter = new ArrayAdapter<ListItem>(this, android.R.layout.activity_list_item, android.R.id.text1, new ArrayList<ListItem>()) {
            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                ListItem item = getItem(position);

                View view = super.getView(position, convertView, parent);

                ImageView image = view.findViewById(android.R.id.icon);
                TextView text1 = view.findViewById(android.R.id.text1);

                image.setImageResource(R.drawable.ic_place_primary_24dp);

                text1.setTextSize(14);
                text1.setTypeface(null, Typeface.BOLD);
                text1.setLineSpacing(1.3f, 1.1f);

                if (item != null) {
                    if (item.isHeader()) {
                        text1.setText(item.getTitle());
                    } else {
                        LogItem log = item.getLog();
                        GeoPoint loc = log.getLoc();

                        String latitude;
                        String longitude;
                        if (loc == null) {
                            latitude = "-";
                            longitude = "-";
                            image.setVisibility(View.GONE);
                        } else {
                            latitude = String.valueOf(loc.getLatitude());
                            longitude = String.valueOf(loc.getLongitude());
                            image.setVisibility(View.VISIBLE);
                        }

                        text1.setText(log.getType());
                        text1.setText(String.format(
                                "id: %s\n" +
                                "type: %s\n" +
                                "imei: %s\n" +
                                "number: %s\n" +
                                "date: %s\n" +
                                "latitude: %s\n" +
                                "longitude: %s\n",
                                log.getId(),
                                log.getType(),
                                log.getImei(),
                                log.getNumber(),
                                log.getDate().toDate().toString(),
                                latitude,
                                longitude));
                    }
                }

                return view;
            }
        };

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ListItem item = (ListItem) adapter.getItem(i);
                if (item != null && item.getLog().getLoc() != null) {
                    GeoPoint loc = item.getLog().getLoc();

                    assert gMap != null;
                    gMap.animateCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(loc.getLatitude(), loc.getLongitude()),
                                    15
                            )
                    );
                }
            }
        });

        app.getDb()
                .collection("logs")
                .orderBy("date", Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

                if (e != null || queryDocumentSnapshots == null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }

                adapter.clear();

                for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                    documentHandler(document);
                }

                adapter.notifyDataSetChanged();
                populateMarker();
            }
        });

        lv.setAdapter(adapter);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }

    private void documentHandler(QueryDocumentSnapshot document) {
        adapter.add(new ListItem(new LogItem(
                document.getId(),
                String.valueOf(document.getData().get("imei")),
                String.valueOf(document.getData().get("type")),
                String.valueOf(document.getData().get("number")),
                (GeoPoint) document.getData().get("loc"),
                (Timestamp) document.getData().get("date")
        )));
    }

    private void populateMarker() {
        if (gMap == null || adapter.getCount() == 0) {
            return;
        }

        for (int i = 0; i < adapter.getCount(); i++) {
            ListItem item = (ListItem) adapter.getItem(i);

            assert item != null;
            assert item.getLog() != null;

            GeoPoint loc = item.getLog().getLoc();

            if (loc != null) {
                LatLng pos = new LatLng(loc.getLatitude(), loc.getLongitude());
                gMap.addMarker(new MarkerOptions()
                        .position(pos)
                        .title(item.getLog().getId())
                );
            }

        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        gMap.setOnMarkerClickListener(this);

        // INDONESIA
        LatLng latLng = new LatLng(-1.3467438,116.528271);
        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 3.5f));

        populateMarker();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
//        SEND MAP INTENT
//        Uri uri = Uri.parse(String.format(
//                Locale.getDefault(),
//                "https://www.google.com/maps/search/?api=1&query=%f,%f",
//                marker.getPosition().latitude, marker.getPosition().longitude)
//        );
//
//        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
//        getApplicationContext().startActivity(intent);

        return false;
    }
}
