package dev.tetamba.callloggerreader;

import android.app.Application;
import android.content.Context;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

public class BaseApp extends Application {
    private static BaseApp instance;
    private FirebaseFirestore db;

    public void onCreate() {
        super.onCreate();

        instance = this;
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        db.setFirestoreSettings(settings);
    }

    public static BaseApp getInstance() {
        return instance;
    }

    public Context getAppContext() {
        return getApplicationContext();
    }

    public FirebaseFirestore getDb() {
        return db;
    }
}
