package dev.tetamba.callloggerreader.models;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.GeoPoint;

import java.util.Date;

public class LogItem {
    private String id;
    private String imei;
    private String type;
    private String number;
    private GeoPoint loc;
    private Timestamp date;

    public LogItem(String id, String imei, String type, String number, GeoPoint loc, Timestamp date) {
        this.id = id;
        this.imei = imei;
        this.type = type;
        this.number = number;
        this.loc = loc;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public String getImei() {
        return imei;
    }

    public String getType() {
        return type;
    }

    public String getNumber() {
        return number;
    }

    public GeoPoint getLoc() {
        return loc;
    }

    public Timestamp getDate() {
        return date;
    }
}
