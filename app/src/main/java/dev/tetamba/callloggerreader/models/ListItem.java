package dev.tetamba.callloggerreader.models;

public class ListItem {
    private String title;
    private LogItem log;

    public ListItem(String title) {
        this.title = title;
    }

    public ListItem(LogItem log) {
        this.log = log;
    }

    public boolean isHeader() {
        return this.log == null;
    }

    public String getTitle() {
        return title;
    }

    public LogItem getLog() {
        return log;
    }
}
